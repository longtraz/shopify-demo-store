export const meta = () => {
  return [
    {title: 'Hydrogen'},
    {description: 'A custom storefront powered by Hydrogen'},
  ];
};
const Index = () => {
  return (
    <div>
      <h3>Hello from the home page!</h3>
    </div>
  );
};

export default Index;
